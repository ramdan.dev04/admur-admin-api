const { decodeToken } = require("../function/helper");
const { Users } = require("../models/models");

exports.Login = async (req, res) => {
    let token = req.headers.authorization
    token = token.replace("Bearer ", "");
    let data = await decodeToken(token)
    if(data) {
        let user = await Users.findOne({email: data.email})
        if(user) {
            res.json({code: 200, from: 1})
        }else{
            let start = new Users({email: data.email});
            await start.save()
            res.json({code: 200})
        }
    }else{
        res.status(500)
        return res.json({code: 500})
    }
}