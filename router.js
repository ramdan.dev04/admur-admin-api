const express = require('express')
const { Login } = require('./api/api')
const route = express.Router()

route.post('/login', Login)

module.exports = route