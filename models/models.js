const mongoose = require('mongoose')

const Schema = mongoose.Schema

const userSchema = new Schema({
    email: String,
    class: {
        type: String, default: "user"
    }
})

const licenseSchema = new Schema({
    email: String,
    registered: String,
    expired: String,
    key: String
})

const serverSchema = new Schema({
    email: String,
    key: String,
    domain: String,
    ip: String,
  });


exports.Users = mongoose.model('users', userSchema)
exports.Licenses = mongoose.model('licenses', licenseSchema)
exports.Servers = mongoose.model('servers', serverSchema)