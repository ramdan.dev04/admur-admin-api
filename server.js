const https = require('https');
const app = require('.');
const fs = require('fs')

let opt = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}

https.createServer(opt, app).listen(443).on('listening', () => {
    console.log('ssl server running')
})