const express = require('express');
const {json, urlencoded} = require('body-parser')
const cookieParser = require('cookie-parser')
const session = require('express-session');
const mongoose = require('mongoose');
const { ApolloServer } = require('apollo-server-express');
const { typeDefs, resolvers } = require('./models/schema');
require('dotenv').config({path: 'devme.env'})
const cors = require('cors');
const route = require('./router');
const { decodeToken } = require('./function/helper');
const { Users } = require('./models/models');

mongoose.connect(process.env.DBURI, (err) => {
    if(err) {
        console.log('Failed connecting to database')
    }
})

const app = express();

app.use(cors())
app.use(json())
app.use(urlencoded({extended: false}));
app.use(cookieParser())
app.use(session({
    secret: `${process.env.SECRET}`,
    resave: false,
    saveUninitialized: false,
}))

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async({req}) => {
    let token = req.headers.authorization || '';
    token = token.replace("Bearer ", '')
    let data = await decodeToken(token)
    // console.log(data)
    if(!data) return false
    let user = await Users.findOne({email: data.email})
    // console.log(user)
    if(!user) return false
    // console.log(user)
    return user
  }
});

server.applyMiddleware({ app });

app.use('/', route);

app.listen({ port: 80 }, () =>
  console.log(`🚀 http Server ready`)
)

module.exports = app
